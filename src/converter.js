
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}
const binpad = (bin) =>{
    while (bin.length < 8) {
        bin = "0"+bin;
    }
    return (bin);
}

module.exports = {
    rgbToHex: (r,g,b) => {
        const rh = r.toString(16);
        const gh = g.toString(16);
        const bh = b.toString(16);
        return (pad(rh)+pad(gh)+pad(bh)).toLowerCase();
    },

    hexToRgb: (red,green,blue) => {
        const r = parseInt(red,16).toString();
        const g = parseInt(green,16).toString();
        const b = parseInt(blue,16).toString();
        return (r+","+g+","+b);
    },

    hexToBin: (hr,hg,hb) => {
        const redbin = parseInt(hr,16).toString(2);
        const greenbin = parseInt(hg,16).toString(2);
        const blubin = parseInt(hb,16).toString(2);

        let bin = ""+binpad(redbin)+" "+binpad(greenbin)+" "+binpad(blubin);

        return (bin);

    },
}

