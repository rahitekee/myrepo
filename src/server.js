const {request} = require('express');
const express = require('express');
const converter = require("./converter");
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Hello"));

app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue);
    res.send(converter.rgbToHex(red,green,blue));
});
app.get('/hex-to-rgb', (req, res) => {
    const red = (req.query.red);    
    const green = (req.query.green);
    const blue = (req.query.blue);
    res.send(converter.hexToRgb(red,green,blue));
});

app.get('/hex-to-bin', (req, res) => {
    const red = (req.query.red);
    const green = (req.query.green);
    const blue = (req.query.blue);
    res.send(converter.hexToBin(red,green,blue));
});

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log(`Server: localhost: ${port}`));
}