const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color code converter", () => {
    describe("RGB to Hex", () => {
        it("converts basic RGB colors", () => {
            const rh = converter.rgbToHex(255,0,0);
            const gh = converter.rgbToHex(0,255,0);
            const bh = converter.rgbToHex(0,0,255);
            
            expect(rh).to.equal("ff0000");
            expect(gh).to.equal("00ff00");
            expect(bh).to.equal("0000ff");

            let h = converter.rgbToHex(10,11,12);
            expect(h).to.equal("0a0b0c");
        });
    });
    describe("Hex to RGB", () => {
        it("converts to basic hex values", () => {
            let rgb = converter.hexToRgb("ff","ff","ff"); 
            expect(rgb).to.equal("255,255,255");
            rgb = converter.hexToRgb("00","00","00"); 
            expect(rgb).to.equal("0,0,0");
        });
    });
    describe("Hex to Binary", () => {
        it("converts hex to binary values", () =>{
            let bin = converter.hexToBin("ff","ff","ff"); 
            expect(bin).to.equal("11111111 11111111 11111111");
            bin = converter.hexToBin("00","04","ff"); 
            expect(bin).to.equal("00000000 00000100 11111111");
        });
    });
});