// Integration test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {

    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });

    describe("RGB to Hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            });
        });
    });
    describe("Hex to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?red=ff&green=ff&blue=ff`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in RGB", (done) => {
            request(url, (error, response, body) => {
                // expect(body).to.equal([255,255,255]);
                expect(body).to.equal("255,255,255");
                done();
            });
        });
    });
    describe("Hex to Binary conversion", () => {
        const url = `http://localhost:${port}/hex-to-bin?red=ff&green=ff&blue=ff`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) =>{
                expect(body).to.equal("11111111 11111111 11111111");
                done();
            });
        });
    });
    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});